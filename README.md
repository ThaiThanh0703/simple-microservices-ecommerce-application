# simple-microservices-ecommerce-application-proxy



Apache HTTP is used to provide the web page of the demo at port 8080. It also forwards HTTP requests to the microservices. This is not really necessary as each service has its own port on the Minikube host but it provides a single point of entry for the whole system. Apache HTTP is configured as a reverse proxy for this. Load balancing is left to Kubernetes.

To configure this Apache HTTP needs to get all registered services from Kubernetes. It just uses DNS for that.