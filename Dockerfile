FROM httpd:2-alpine

# Copy config and html files 
COPY index.html /usr/local/apache2/htdocs/
COPY 000-default.conf /usr/local/apache2/conf/

EXPOSE 80

CMD ["httpd", "-D", "FOREGROUND"]